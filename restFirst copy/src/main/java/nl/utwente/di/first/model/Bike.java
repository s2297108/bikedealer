package nl.utwente.di.first.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement

public class Bike {

    private String bikeID;
    private String ownerName;
    private String color;
    private String gender;

    public Bike(){}

    public Bike(String id, String ownerName, String color, String gender){
        this.bikeID = id;
        this.color = color;
        this.ownerName = ownerName;
        this.gender = gender;
    }

    public String getBikeID() {
        return bikeID;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getColor() {
        return color;
    }

    public String getGender() {
        return gender;
    }

    public void setBikeID(String bikeID) {
        this.bikeID = bikeID;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }
}
