package nl.utwente.di.first.resources;

import nl.utwente.di.first.dao.BikeDao;
import nl.utwente.di.first.model.Bike;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Path("/bikes")
public class BikesResource {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;

    @POST
    @Produces(MediaType.TEXT_HTML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void createBike(@FormParam("id") String id, @FormParam("ownerName") String ownerName, @FormParam("color") String color, @FormParam("gender") String gender, @Context HttpServletResponse servletResponse) throws IOException {
        Bike bike = new Bike(id, ownerName, color, gender);
        BikeDao.instance.getModel().put(id, bike);
        servletResponse.sendRedirect("../create_bike.html");
    }

    @GET
    @Produces(MediaType.TEXT_XML)
    public List<Bike> getBikesBrowser() {
        List<Bike> bikes = new ArrayList<Bike>();
        bikes.addAll(BikeDao.instance.getModel().values());
        return bikes;
    }

    //need to be filtered by color=x and gender=y
    //<base>/bikes?color=x&gender=y -> website
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Bike> getBikes() {
        List<Bike> bikes = new ArrayList<Bike>();
        bikes.addAll(BikeDao.instance.getModel().values());
        return bikes;
    }

    @Path("{bikeId}")
    public BikeResource getBikeDetails(@PathParam("bikeId") String id){
        return new BikeResource(uriInfo, request, id);
    }


    //Returns a message indicating success or fail.
    @GET
    @Path("order")
    @Produces(MediaType.TEXT_PLAIN)
    public String orderBike() {
        int orders = BikeDao.instance.getModel().size();
        return String.valueOf(orders);
    }


}
