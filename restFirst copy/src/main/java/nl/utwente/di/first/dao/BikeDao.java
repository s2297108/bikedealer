package nl.utwente.di.first.dao;
import java.util.HashMap;
import java.util.Map;
import nl.utwente.di.first.model.Bike;

public enum BikeDao {

    instance;

    private Map<String, Bike> contentProvider = new HashMap<String, Bike>();

    private BikeDao() {

        Bike bike = new Bike("1", "Sara", "purple", "female");
        //Bike bike = new Bike("1", "owner1", "red", "female");
        contentProvider.put("1", bike);
//        bike = new Bike("2", "owner2", "blue", "female");
//        contentProvider.put("2", bike);

    }

    public Map<String, Bike> getModel() {
        return contentProvider;
    }
}
